## 1.2.0

- Allow single versions to fail building

## 1.0.0

- Initial version.
