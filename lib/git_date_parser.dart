DateTime gitDateParser(String output) {
  return DateTime.fromMillisecondsSinceEpoch(int.parse(output) * 1000);
}
