import 'dart:convert';
import 'dart:io';

import 'package:json_annotation/json_annotation.dart';
import 'package:version/version.dart';
import 'package:yaml/yaml.dart';

import 'git_date_parser.dart';

part 'version_model.g.dart';

@JsonSerializable()
class VersionModel implements Comparable<VersionModel> {
  final String version;
  final Map<String, dynamic> pubspec;
  @JsonKey(name: 'archive_url')
  final String archiveUrl;
  final DateTime published;

  VersionModel({
    required this.version,
    required this.pubspec,
    required this.archiveUrl,
    required this.published,
  });

  factory VersionModel.fromJson(Map<String, dynamic> json) =>
      _$VersionModelFromJson(json);

  Map<String, dynamic> toJson() => _$VersionModelToJson(this);

  static Future<VersionModel> fromGit(
      String ref, String baseUri, String outputDir) async {
    final dateOutput = await Process.run(
        'git', ['--no-pager', 'show', '-s', '--format=%ct', ref]);
    final timestamp = gitDateParser(dateOutput.stdout);
    final pubspecOutput =
        await Process.run('git', ['--no-pager', 'show', '$ref:./pubspec.yaml']);
    final pubspec = loadYaml(pubspecOutput.stdout) as YamlMap;
    final version = pubspec['version'] as String;
    final name = pubspec['name'] as String;

    final directory = Directory('$outputDir/packages/$name/versions/');
    directory.create(recursive: true);

    final path = '/packages/$name/versions/$version.tar';

    await Process.run(
        'git', ['archive', '--output=$outputDir$path', '--format=tar', ref]);

    await Process.run('gzip', ['$outputDir$path']);

    final archiveUrl = baseUri + path;
    return VersionModel(
      version: version,
      pubspec: jsonDecode(json.encode(pubspec)) as Map<String, dynamic>,
      archiveUrl: archiveUrl + '.gz',
      published: timestamp,
    );
  }

  @override
  int compareTo(VersionModel other) =>
      Version.parse(version).compareTo(Version.parse(other.version));
}
