// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ApiDataModel _$ApiDataModelFromJson(Map<String, dynamic> json) => ApiDataModel(
      name: json['name'] as String,
      latest: VersionModel.fromJson(json['latest'] as Map<String, dynamic>),
      versions: (json['versions'] as List<dynamic>)
          .map((e) => VersionModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ApiDataModelToJson(ApiDataModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'latest': instance.latest,
      'versions': instance.versions,
    };
