import 'dart:developer';
import 'dart:io';

import 'package:json_annotation/json_annotation.dart';
import 'package:version/version.dart';

import 'version_model.dart';

part 'api_data_model.g.dart';

@JsonSerializable()
class ApiDataModel {
  final String name;
  final VersionModel latest;
  final List<VersionModel> versions;

  ApiDataModel({
    required this.name,
    required this.latest,
    required this.versions,
  });

  factory ApiDataModel.fromJson(Map<String, dynamic> json) =>
      _$ApiDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$ApiDataModelToJson(this);

  static Future<ApiDataModel> fromGit(String baseUri, String outputDir) async {
    final result = await Process.run('git', ['--no-pager', 'tag', '-l']);
    final tags = result.stdout.toString().split('\n')..removeLast();

    Map<Version, VersionModel> versions = {};
    for (final ref in tags) {
      try {
        final version = await VersionModel.fromGit(ref, baseUri, outputDir);
        versions[Version.parse(version.version)] = version;
      } catch (e, s) {
        log('Couldn\'t build VersionModel for tag $ref:',
            error: e, stackTrace: s);
      }
    }

    final keys = versions.keys.toList();
    keys.sort();
    final latest = versions[keys.reversed.first]!;

    return ApiDataModel(
        name: latest.pubspec['name'],
        latest: latest,
        versions: versions.values.toList()..sort());
  }
}
