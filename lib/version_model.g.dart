// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'version_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VersionModel _$VersionModelFromJson(Map<String, dynamic> json) => VersionModel(
      version: json['version'] as String,
      pubspec: json['pubspec'] as Map<String, dynamic>,
      archiveUrl: json['archive_url'] as String,
      published: DateTime.parse(json['published'] as String),
    );

Map<String, dynamic> _$VersionModelToJson(VersionModel instance) =>
    <String, dynamic>{
      'version': instance.version,
      'pubspec': instance.pubspec,
      'archive_url': instance.archiveUrl,
      'published': instance.published.toIso8601String(),
    };
