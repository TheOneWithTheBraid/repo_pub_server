# repo_pub_server

Builds a static pub server from the package you develop, enabling you to provide continuous delivery of your
bleeding edge dart package builds.

## Getting started

1. Globally install `repo_pub_server` to your CI environment
2. Install `git` and `gzip` on your environment
3. Add the example code to your CI/CD configuration

## Example code

```bash
# ensure you added the dev_dependency
flutter pub global activate repo_pub_server
cd path/to/your/package
repo_pub_server https://your-final-serving-url.tld/ /directory/to/build/at
```

## How it works

`repo_pub_server` uses git to find all tags. Based on the versions and the corresponding pubspec files, a static
dart package repository is being built.