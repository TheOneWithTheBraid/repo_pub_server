import 'dart:convert';
import 'dart:io';

import 'package:repo_pub_server/api_data_model.dart';

Future<void> main(List<String> arguments) async {
  late String baseUri;
  try {
    baseUri = arguments[0];
  } catch (e) {
    throw 'Please provide the server\'s base URI as first argument.';
  }
  if (baseUri.endsWith('/')) baseUri = baseUri.substring(0, baseUri.length - 1);

  late String outputDir;
  try {
    outputDir = arguments[1];
  } catch (e) {
    throw 'Please provide the output directory as second argument.';
  }

  await Process.run('git', ['fetch']);
  final model = await ApiDataModel.fromGit(baseUri, outputDir);

  await Directory(outputDir + '/api/packages/').create(recursive: true);

  File(outputDir + '/api/packages/${model.name}')
    ..create(recursive: true)
    ..writeAsString(jsonEncode(model.toJson()));
}
